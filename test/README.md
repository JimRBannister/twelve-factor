# Twelve Factors - Integration Testing in Apollo Server v4

- [Getting started](#getting-started)
- 
- [References](#handy-references)

This example demonstrates the basics of testing Apollo Server. [See the docs on integration testing for more information](https://www.apollographql.com/docs/apollo-server/v4/testing/testing/).

Check out the `src/__tests__/server.test.ts` file if you'd like to see how we are defining our test.

## Run locally

To run the tests locally:

```shell
npm install
npm test
```

To run the server locally:

```shell
npm install
npm start
```

## Trouble shooting

You may have problems with supertest. Try reinstalling if you run into difficulties.



## Handy References!


- [Docker](https://www.docker.com/)
- [Docker compose](https://docs.docker.com/compose/)

