# Twelve Factors 2

This is the second part of the Developing & Testing Software-as-a-Service Applications course.

## Introduction

This course demonstrates the [Twelve-Factor App](https://12factor.net/) methodology.

This course can be run on a Windows, Linux or MacOs machine.

## prerequisites.

Rancher Desktop will be used for these exercises. However, Docker Desktop can be used instead if already installed.
Do check first. 

Please install the Rancher Desktop and Postgres

Please install Rancher (if there isn't an alternative such as Docker Desktop)
https://rancherdesktop.io/

Please ensure dockerd is checked and containerd is unchecked under Kubernetes Settings of Rancher Desktop.

https://www.postgresql.org/download/

## Practical.

Please first clone this respository, and then make use of the sub-folders that are part of it. Each sub-folder covers one or more of the Twelve-Factor App factors, and each contains the instructions, source code and configuration files.
