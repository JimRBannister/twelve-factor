import React from 'react';
import { render } from 'react-dom';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql
} from "@apollo/client";

const TEMPERATURES = gql`
  query GetTemperatures {
    allTemperatures {
      temperature
    }
  }
`;

function Temperatures() {
  const { loading, error, data } = useQuery(TEMPERATURES);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

    console.dir(data)
  return data.allTemperatures.map(({ temperature}) => (
      <div key={temperature}>
        <p>
          {temperature}
        </p>
      </div>
  ));
}



export default function App() {
  return (
      <div>
        <Temperatures />
      </div>
  );
}

