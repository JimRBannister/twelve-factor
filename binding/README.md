## ## Twelve Factors - binding

docker build -t hartree/binding:1.0 .

docker run -d -p 5009:5000 hartree/binding:1.0 .


### multiple instances can be started, for example (see the results via your web browser http://localhost:5009 & http://localhost:5010)

docker run -d -p 5010:5000 hartree/binding:1.0 .


### To list containers (note the first column is the container id. The container id can be stop running containers and then remove them after they have been stopped)

docker ps -a


### To stop a running container

docker stop <container-id>


### To remover a container.

docker rm  <container-id>


