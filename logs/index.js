// Twelve Factors logging example

var fs = require('fs');
var util = require('util');

var logFile = fs.createWriteStream(__dirname + '/logger.log', {flags : 'w'});
console.log = (msg) => {
  logFile.write(util.format(msg) + '\n');
};

var cnt = 0;
var log = () => {
  console.log(`log message count ${cnt}`);
  cnt++;
};

setInterval(log, 3000);
