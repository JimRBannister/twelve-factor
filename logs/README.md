## Twelve Factors - logs

docker build -t hartree/logs:1.0 .

docker run -dp 3000:3000 hartree/logs:1.0

### use docker ps to find the logs container

docker ps

docker logs <container-id>

